-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Waktu pembuatan: 18 Mar 2021 pada 14.38
-- Versi server: 10.1.39-MariaDB
-- Versi PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_evermostest`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_product`
--

CREATE TABLE `tm_product` (
  `product_id` int(11) NOT NULL,
  `sku` varchar(25) NOT NULL,
  `product_name` varchar(75) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_product`
--

INSERT INTO `tm_product` (`product_id`, `sku`, `product_name`, `price`, `stock`, `created_at`) VALUES
(1, '0789AS', 'Sepatu ', 120000, 40, '2021-03-18 14:02:55'),
(2, '0678QW', 'Baju', 50000, 5, '2021-03-18 13:22:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttr_orderdetail`
--

CREATE TABLE `ttr_orderdetail` (
  `orderdetail_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttr_orderdetail`
--

INSERT INTO `ttr_orderdetail` (`orderdetail_id`, `order_id`, `product_id`, `qty`) VALUES
(1, 1, 1, 10),
(2, 1, 2, 5);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tm_product`
--
ALTER TABLE `tm_product`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indeks untuk tabel `ttr_orderdetail`
--
ALTER TABLE `ttr_orderdetail`
  ADD PRIMARY KEY (`orderdetail_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tm_product`
--
ALTER TABLE `tm_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ttr_orderdetail`
--
ALTER TABLE `ttr_orderdetail`
  MODIFY `orderdetail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
