1. Hasil analisa saya dari hasil fakta-fakta yang disampaikan, ulasan buruk tersebut bisa terjadi karena kekecewaan pelanggan yang merasa sudah berhasil melakukan pemesanan dan sudah melakukan pembayaran pesanan namun dibatalkan karena tidak tersedianya stok. 
Pemesanan tersebut bisa terjadi karena masih dibolehkannya user oleh sistem untuk melakukan pesanan disaat stok sudah habis. Karena tidak adanya aktifitas pengecekan inventori oleh FE dan juga intervensi dari BE untuk menolak melakukan pemesanan sehingga menyebabkan pencatatan pada transaksi inventaris bisa menghasilkan negatif. Padahal kenyataannya stok sudah habis tetapi user berhasil melakukan checkout dan juga pembayaran.

2. Untuk mencegah masalah tersebut terulang kembali, perlu adanya API/fungsi dari BE untuk melakukan pengecekan sisa stok pada sebuah item. Apabila stok sudah tidak mencukupi maka BE bisa memberikan notifikasi untuk intervensi FE memberhentikan aktifitas user melakukan transaksi pada item tersebut. Seperti tidak membolehkan user memesan item lebih banyak dari stok yang tersedia dan pemberitahuan pada user bahwa item tersebut telah habis/tidak mencukupi serta menonaktifkan tombol pesan pada item tersebut di interface user. 

3. Penyelesaian Secara Teknis

Dalam implementasi solusi diatas, akan dibutuhkan 2 API dan juga melibatkan 2 tabel. Yaitu tabel produk dan tabel order detail.

Check Stock API
- Fungsi = API untuk melakukan pengecekan stok yang tersedia pada sebuah produk, dapat diimplementasikan pada FE sebagai info jumlah stok dan juga trigger untuk FE menonaktifkan button Order apabila stok tidak tersedia.
- EndPoints = http://localhost/evermos-test1/check-stock
- Method = POST
- Parameter = # product_id => Sesuai produk yang tersedia

Added Product to Order Detail API
- Fungsi = API untuk submit order detail apabila stok pada produk tersedia, sistem dapat menolak apabila kuantitas produk yang dipesan melebihi stok yang tersedia. (Asumsi bahwa untuk proses submit order sudah dilakukan.)
- EndPoints = http://localhost/evermos-test1/order-detail
- Method = POST
- Parameter = # product_id => Sesuai produk yang akan dipesan; # qty
