<?php

defined('BASEPATH') OR exit('No direct script access allowed');

    class C_product extends MY_Controller {
		
		function __construct($config = 'rest') {
			parent::__construct($config);
			$this->load->database();
            $this->load->model("M_product");
		}

		public function checkStock_post(){
			$params   = $_REQUEST;
            $this->form_validation->set_rules("product_id", "product_id", "required|numeric");
			
			if ($this->form_validation->run() === FALSE){
				
				$respond["status"]  					= FALSE;
				$respond["header"]  					= REST_Controller::HTTP_NOT_FOUND;
				$respond["message_system"] 				= "error input";
				$respond["data"]    					= ["message"		=> explode("\n", strip_tags(validation_errors())) ? explode("\n", strip_tags(validation_errors())) : $this->session->flashdata("message")];
				
				$this->displayToJSON($respond);
			} else {
				$params   	    = $_REQUEST;

                //Mengambil Data Produk
				$get_product = $this->M_product->getAllData(["product_id" => $params["product_id"]])->first_row();
               
                if(!empty($get_product)){
                    if($get_product->stock <= 0) {
                        //Pengecekan Apabila Stok Pada Produk Tidak Tersedia
                        $data_respond = [
                                            "message"				=> "Stok tidak tersedia"
                                        ];
                        $respond["status"] 			= FALSE;
                        $respond["header"]			= REST_Controller::HTTP_OK;
                        $respond["message_system"]	= "product out of stock";
                        $respond["data"]    		= $data_respond;
                        $this->displayToJSON($respond);
                    } else {
                        $data_respond		= [
                                                "product"				=> $get_product,
                                            ];
                        $respond["status"] 			= TRUE;
                        $respond["header"]			= REST_Controller::HTTP_OK;
                        $respond["message_system"]	= "success get product";
                        $respond["data"]			= $data_respond;
                        // Return Apabila Stock Tersedia
                        $this->displayToJSON($respond);
                    }
                } else {    
                    //Return Apabila Produk Tidak Ditemukan
                    $this->displayDataNotFound("product not found", "produk tidak ditemukan");
                }
			}
		}
    }
    ?>