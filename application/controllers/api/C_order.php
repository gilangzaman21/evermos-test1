<?php

defined('BASEPATH') OR exit('No direct script access allowed');

    class C_order extends MY_Controller {
		
		function __construct($config = 'rest') {
			parent::__construct($config);
			$this->load->database();
            $this->load->model(["M_orderdetail", "M_product"]);
		}

		public function orderDetail_post(){
			$params   = $_REQUEST;
            $this->form_validation->set_rules("product_id", "product_id", "required|numeric");
            $this->form_validation->set_rules("qty", "qty", "required|numeric");
			
			if ($this->form_validation->run() === FALSE){
				
				$respond["status"]  					= FALSE;
				$respond["header"]  					= REST_Controller::HTTP_NOT_FOUND;
				$respond["message_system"] 				= "error input";
				$respond["data"]    					= ["message"		=> explode("\n", strip_tags(validation_errors())) ? explode("\n", strip_tags(validation_errors())) : $this->session->flashdata("message")];
				
				$this->displayToJSON($respond);
			} else {
				$params   	    = $_REQUEST;

                //Mengambil Data Produk
				$get_product = $this->M_product->getAllData(["product_id" => $params["product_id"]])->first_row();

                //Perhitungan Sisa Stock
                $sisa_stock = $get_product->stock - $params["qty"];
               
                if(!empty($get_product)){
                    if($sisa_stock < 0) {
                        //Pengecekan Apabila Stok Pada Produk Tidak Tersedia
                        $data_respond = [
                                            "message"				=> "Stok tidak tersedia"
                                        ];
                        $respond["status"] 			= FALSE;
                        $respond["header"]			= REST_Controller::HTTP_OK;
                        $respond["message_system"]	= "product out of stock";
                        $respond["data"]    		= $data_respond;
                        $this->displayToJSON($respond);
                    } else {
                        //Insert Detail Product
                        $data_insert = [
                                            "order_id"      => 1,  
                                            "product_id"    => $params["product_id"],
                                            "qty"           => $params["qty"]
                                    ];
                        $insert      = $this->M_orderdetail->insert($data_insert);

                        //Update Stock Product
                        $data_update = ["stock"    => $sisa_stock];
                        $update      = $this->M_product->update($data_update, ["product_id"   => $params["product_id"]]);

                        $data_respond		= [
                                                "message"    => "sukses menambahkan produk ke order detail"
                                            ];
                        $respond["status"] 			= TRUE;
                        $respond["header"]			= REST_Controller::HTTP_OK;
                        $respond["message_system"]	= "success added product to order detail";
                        $respond["data"]			= $data_respond;
                        // Return Apabila Sukses Menambahkan Detail Order
                        $this->displayToJSON($respond);
                    }
                } else {    
                    //Return Apabila Produk Tidak Ditemukan
                    $this->displayDataNotFound("product not found", "produk tidak ditemukan");
                }
			}
		}
    }
    ?>