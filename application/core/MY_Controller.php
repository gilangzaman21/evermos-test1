<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends REST_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function displayToJSON($content) {
		header("Content-type:application/json;charset=utf8;");
		exit($this->response($content,REST_Controller::HTTP_OK));
	}
	
	public function displayBadRequest($content) {
		header("Content-type:application/json;charset=utf8;");
		exit($this->response($content,REST_Controller::HTTP_BAD_REQUEST));
    }

    public function displayDataNotFound($message_system = 'data does not exists', $message = 'data does not exists') {
		$data = ["message"	=> $message];

		header("Content-type:application/json;charset=utf8;");

		exit($this->response([
			"status" 			=> FALSE,
			"header" 			=> REST_Controller::HTTP_NOT_FOUND,
			"message_system" 	=> $message_system,
			"data"				=> $data
		],REST_Controller::HTTP_NOT_FOUND));
    }
}