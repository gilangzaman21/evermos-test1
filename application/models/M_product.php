<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends MY_Model{
	
	protected $tableName= "tm_product";
    public $primaryKey = "product_id";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

    }
}
?>