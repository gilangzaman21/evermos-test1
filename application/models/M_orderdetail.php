<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_orderdetail extends MY_Model{
	
	protected $tableName= "ttr_orderdetail";
    public $primaryKey = "orderdetail_id";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();

    }
}
?>