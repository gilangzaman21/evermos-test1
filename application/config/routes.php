<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*### API ###*/

$route['check-stock']['POST'] = 'api/C_product/checkStock';
$route['order-detail']['POST'] = 'api/C_order/orderDetail';